<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index');
Route::get('/register', 'AuthController@bio');
Route::post('/welcome', 'AuthController@kirim');
Route::get('/data-table', 'IndexController@table');


//CRUD Cast
//create
Route::get('/cast/create', 'CastController@create'); //mengarah ke form tambah data 
Route::post('/cast', 'CastController@store'); //menyimpan data form ke database table cast

//read
Route::get('/cast', 'CastController@index'); // ambil data ke database kemudian tampil di blade
Route::get('/cast/{cast_id}', 'CastController@show');

//update
Route::get('/cast/{cast_id}/edit', 'CastController@edit'); //mengarah form edit
Route::put('/cast/{cast_id}', 'CastController@update');

//delete
Route::delete('/cast/{cast_id}', 'CastController@destroy');
