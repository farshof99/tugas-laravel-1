<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
   public function bio(){
       return view('page.daftar');
   }

   public function kirim(Request $request){
      // dd($request->all());
      $firstname = $request['fname'];
      $lastname = $request['lname'];
      $jeniskelamin = $request['jk'];
      $bangsa = $request['nationality'];
     
      return view('page.welcome', compact("firstname", "lastname","jeniskelamin", "bangsa"));

   }
}
