@extends('layout.master')
@section('title')
Halaman pendaftaran
@endsection
@section('content')
   
        <form action="/welcome" method="POST">
            @csrf
            <label>First name</label><br>
            <input type="text" name="fname"><br><br>
            <label>Last name</label><br>
            <input type="text" name="lname"><br><br>
            <label>Gender</label><br>
            <input type="radio" value="Laki-Laki" name="jk">Laki-laki<br>
            <input type="radio" value="Perempuan" name="jk">Perempuan<br><br>
            <label>Nationality</label> <br>
            <select name="nationality">
                <option value="Indonesia">Indonesia</option>
                <option value="Amerika">Amerika</option>
                <option value="Inggris">Inggris</option>
            </select> <br><br>
            <label>Language Spoken</label> <br>
            <input type="checkbox" value="1">Bahasa Indonesia<br>
            <input type="checkbox" value="2">English<br>
            <input type="checkbox" value="3">Other<br><br>
            <label>Bio</label><br>
            <textarea name="bio" cols="30" rows="10"></textarea> <br><br>




            <input type="Submit" value="Sign up">
        </form>
    <a href="/">Kembali ke halaman home</a>
@endsection